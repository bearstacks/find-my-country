//
//  AppDelegate.h
//  HelloEarth
//
//  Created by Rehan Anwar on 2016-04-25.
//  Copyright © 2016 Rehan Anwar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

