//
//  ViewController.m
//  HelloEarth
//
//  Created by Rehan Anwar on 2016-04-25.
//  Copyright © 2016 Rehan Anwar. All rights reserved.
//

#import "ViewController.h"

MaplyVectorObject *actualCountry;
MaplyVectorObject *selectedCountry;
int value = 0;
int totalPossible = 1000;
double kilometers = 0;
CGRect labelsize;
CGRect labelsize2;
CGRect labelsize3;
NSTimer *tensectimer;
NSTimer *onesectimer;
int number_of_countries = 0;
bool gameOver = false;
bool test = false;
int time_Sec = 10;
double animation_time = 2.0;
MaplyLabel *actualCountryLabel;
MaplyLabel *selectedCountryLabel;
NSArray *labels;
UIButton *startGame;
int gamenumber = 10;

bool updateTime = false;


bool selected = false;

@interface ViewController ()

-(void) addCountries;
-(void) addAnnotation:(NSString *)title withSubtitle:(NSString *)subtitle at: (MaplyCoordinate)coord;
-(void) newCountry;
-(void) updateScore;
-(void) update:(BOOL)usertapped;
-(void) reset_Timer;
-(void) sec_update;
-(void) startGame;
-(void) addlabelsWithActualCountry:(NSString *)actualCountry withCoordinates:(MaplyCoordinate)actualcoord
                   selectedCountry:(NSString *)selectedCountry withCoordinates:(MaplyCoordinate)selectedcoord;


@end

@implementation ViewController
{
    WhirlyGlobeViewController *theViewC;
    NSDictionary *vectorDict;
    NSDictionary *vectorDict2;
    NSDictionary *setupVectorDict;
    UILabel *Country;
    UILabel *Score;
    UILabel *Timing;
    UILabel *Instructions;
    MaplyComponentObject *highlightedSelectedCountry;
    MaplyComponentObject *highlightedActualCountry;
    MaplyComponentObject *countrylabels;

    //MaplyComponentObject *highlightedCountry;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Create an empty globe and add it to the view
    theViewC = [[WhirlyGlobeViewController alloc] init];
    [self.view addSubview:theViewC.view];
    theViewC.view.frame = self.view.bounds;
    [self addChildViewController:theViewC];
    
    // this logic makes it work for either globe or map
    WhirlyGlobeViewController *globeViewC = nil;
    MaplyViewController *mapViewC = nil;
    if ([theViewC isKindOfClass:[WhirlyGlobeViewController class]])
        globeViewC = (WhirlyGlobeViewController *)theViewC;
    else
        mapViewC = (MaplyViewController *)theViewC;
    
    // we want a black background for a globe, a white background for a map.
    theViewC.clearColor = (globeViewC != nil) ? [UIColor blackColor] : [UIColor whiteColor];
    
    // and thirty fps if we can get it ­ change this to 3 if you find your app is struggling
    theViewC.frameInterval = 2;
    
    // set up the data source
    MaplyMBTileSource *tileSource =
    [[MaplyMBTileSource alloc] initWithMBTiles:@"geography-class_medres"];
    
    // set up the layer
    MaplyQuadImageTilesLayer *layer =
    [[MaplyQuadImageTilesLayer alloc] initWithCoordSystem:tileSource.coordSys
                                               tileSource:tileSource];
    layer.handleEdges = (globeViewC != nil);
    layer.coverPoles = (globeViewC != nil);
    layer.requireElev = false;
    layer.waitLoad = false;
    layer.drawPriority = 0;
    layer.singleLevelLoading = false;
    [theViewC addLayer:layer];
    

    
    // Do any additional setup after loading the view, typically from a nib.
    
//    vectorDict = @{
//                   kMaplyColor: [UIColor clearColor],
//                   kMaplySelectable: @(true),
//                   kMaplyVecWidth: @(0.0)};
//    
    vectorDict2 = @{
                   kMaplyColor: [UIColor greenColor],
                   kMaplySelectable: @(true),
                   kMaplyFilled: @(true),
                   kMaplyVecWidth: @(1.0)};
    
    setupVectorDict = @{
                   kMaplyColor: [UIColor clearColor],
                   kMaplySelectable: @(true),
                   kMaplyVecWidth: @(0.0)};
    
    theViewC.tilt= 0;
    theViewC.keepNorthUp = true;
    theViewC.autoMoveToTap = false;
    
    theViewC.delegate=self;
    
    [theViewC setPosition:MaplyCoordinateMakeWithDegrees(360, 360)];
    theViewC.height = 1.0;
    
    
    startGame = [[UIButton alloc] init];
    startGame.frame = CGRectMake(0, 0, 200, 80);
    startGame.titleLabel.font = [UIFont systemFontOfSize:30];
    [startGame setTitle:@"Start Game" forState:UIControlStateNormal];
    [startGame setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [startGame setTitleColor:[UIColor blackColor] forState: UIControlStateHighlighted];
    startGame.backgroundColor = [UIColor grayColor];
    startGame.alpha = 0.5;
    startGame.layer.cornerRadius = 10;
    startGame.titleLabel.clipsToBounds = YES;
    startGame.center = self.view.center;
    startGame.titleLabel.textAlignment = NSTextAlignmentCenter;
    [startGame addTarget:self action:@selector(startGame)  forControlEvents:UIControlEventTouchUpInside];
    
    [theViewC.view addSubview:startGame];
    
//    Instructions = [[UILabel alloc]init];
//    Instructions.text = @"Instructions: Find the Country";
//    Instructions.textColor = [UIColor whiteColor];
//    Instructions.font = [UIFont systemFontOfSize:20];
//    Instructions.textAlignment = NSTextAlignmentCenter;
//    Instructions.adjustsFontSizeToFitWidth = true;
//    Instructions.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.5f];
//    Instructions.frame = CGRectMake(0, 0, 300, 200);
//    Instructions.center = self.view.center;
//    Instructions.layer.cornerRadius = 10;
//    Instructions.clipsToBounds = YES;
//    
//    [theViewC.view addSubview:Instructions];
    
    [theViewC setAutoRotateInterval:0.1 degrees:60];
    
    Score = [[UILabel alloc]init];
}

-(void) startGame
{
    [self addCountries];
    number_of_countries=0;
    value = 0;
    
    gameOver = false;
    [startGame setHidden:true];
    [self performSelector:@selector(stopRotating) withObject:nil afterDelay:0];

    onesectimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(sec_update) userInfo: nil repeats:YES];
    
    [self update:false];
    
    
    int randomCountry = arc4random()%230+1;
    
    NSArray *allOutlines = [[NSBundle mainBundle] pathsForResourcesOfType:@"geojson" inDirectory:@"country_json_50m"];
    
    NSString *outlineFile = allOutlines[randomCountry];
    
    NSData *jsonData = [NSData dataWithContentsOfFile:outlineFile];
    if (jsonData)
    {
        MaplyVectorObject *wgVecObj = [MaplyVectorObject VectorObjectFromGeoJSON:jsonData];
        
        // the admin tag from the country outline geojson has the country name ­ save
        NSString *vecName = [[wgVecObj attributes] objectForKey:@"ADMIN"];
        wgVecObj.userObject = vecName;
        
        Country = [[UILabel alloc]init];
        Country.text = vecName;
        Country.textColor = [UIColor whiteColor];
        Country.font = [UIFont systemFontOfSize:50];
        Country.textAlignment = NSTextAlignmentCenter;
        Country.adjustsFontSizeToFitWidth = true;
        Country.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.5f];
        
        labelsize = [Country.text boundingRectWithSize:CGSizeMake(self.view.frame.size.width, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:50 weight:1.0]} context:nil];
        
        Country.frame = CGRectMake(0, 25, labelsize.size.width, 60);
        Country.center = CGPointMake(theViewC.view.center.x, 50);
        Country.layer.cornerRadius = 10;
        Country.clipsToBounds = YES;
        [theViewC.view addSubview:Country];
    }
    

    Score.text = [NSString stringWithFormat:@"%d  /  %d", value, totalPossible];
    Score.textColor = [UIColor whiteColor];
    Score.font = [UIFont systemFontOfSize:30];
    Score.textAlignment = NSTextAlignmentCenter;
    Score.adjustsFontSizeToFitWidth = true;
    Score.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.5f];
    Score.layer.cornerRadius = 5;
    Score.clipsToBounds = YES;;
    
    labelsize2 = [Score.text boundingRectWithSize:CGSizeMake(theViewC.view.frame.size.width, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:Score.font.pointSize weight:1.0]} context:nil];
    
    Score.text = [NSString stringWithFormat:@"%d  /  %d", value, totalPossible];
    labelsize2 = [Score.text boundingRectWithSize:CGSizeMake(theViewC.view.frame.size.width, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:Score.font.pointSize weight:1.0]} context:nil];
    Score.frame = CGRectMake(0, 0, labelsize2.size.width, labelsize2.size.height);
    Score.center = CGPointMake(theViewC.view.center.x, theViewC.view.frame.size.height*0.9333);
    
    [theViewC.view addSubview:Score];
    
    
    Timing = [[UILabel alloc]init];
    Timing.textColor = [UIColor redColor];
    Timing.font = [UIFont systemFontOfSize:30];
    Timing.textAlignment = NSTextAlignmentCenter;
    Timing.adjustsFontSizeToFitWidth = true;
    //Timing.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.5f];
    Timing.layer.cornerRadius = 10;
    Timing.clipsToBounds = YES;
    
    
    //Timing.text = [NSString stringWithFormat:@"%d", time_Sec];
    labelsize3 = [Timing.text boundingRectWithSize:CGSizeMake(theViewC.view.frame.size.width, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:Timing.font.pointSize weight:1.0]} context:nil];
    
    Timing.frame = CGRectMake(0, 0, labelsize3.size.width, labelsize3.size.height);
    Timing.center = CGPointMake(theViewC.view.frame.size.width*0.9, theViewC.view.frame.size.height*0.9333);
    
    [theViewC.view addSubview:Timing];
    
    theViewC.tilt= 0;
    theViewC.keepNorthUp = true;
    theViewC.autoMoveToTap = false;
    
    theViewC.delegate=self;
    
    [theViewC setPosition:MaplyCoordinateMakeWithDegrees(360, 360)];
    theViewC.height = 1.0;

}

-(void)stopRotating
{
    [theViewC setAutoRotateInterval:0.0 degrees:0];
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void) idontknow
{
    onesectimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(sec_update) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)addCountries
{
    // handle this in another thread
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0),
                   ^{
                       NSArray *allOutlines = [[NSBundle mainBundle] pathsForResourcesOfType:@"geojson" inDirectory:@"country_json_50m"];
                       
                       for (NSString *outlineFile in allOutlines)
                       {
                           NSData *jsonData = [NSData dataWithContentsOfFile:outlineFile];
                           if (jsonData)
                           {
                               MaplyVectorObject *wgVecObj = [MaplyVectorObject VectorObjectFromGeoJSON:jsonData];
                               
                               // the admin tag from the country outline geojson has the country name ­ save
                               NSString *vecName = [[wgVecObj attributes] objectForKey:@"ADMIN"];
                               wgVecObj.userObject = vecName;
                               
 
                               
                               // add the outline to our view
                               MaplyComponentObject *compObj = [theViewC addVectors:[NSArray arrayWithObject:wgVecObj] desc:setupVectorDict];
                               
                               // If you ever intend to remove these, keep track of the MaplyComponentObjects above.
                               
//                               if ([vecName length] > 0)
//                               {
//                                   MaplyScreenLabel *label = [[MaplyScreenLabel alloc] init];
//                                   label.text = vecName;
//                                   label.loc = [wgVecObj center];
//                                   label.selectable = true;
//                                   label.layoutImportance = 10.0;
//                                   [theViewC addScreenLabels:@[label] desc:
//                                    @{
//                                      kMaplyFont: [UIFont boldSystemFontOfSize:5.0],
//                                      kMaplyTextOutlineColor: [UIColor blackColor],
//                                      kMaplyTextOutlineSize: @(2.0),
//                                      kMaplyColor: [UIColor whiteColor]
//                                      }];
//                               }
                           }
                       }
                   });
}


// Unified method to handle the selection
- (void) handleSelection:(MaplyBaseViewController *)viewC
                selected:(NSObject *)selectedObj
{
    // ensure it's a MaplyVectorObject. It should be one of our outlines.
//    if ([selectedObj isKindOfClass:[MaplyVectorObject class]])
//    {
//        MaplyVectorObject *theVector = (MaplyVectorObject *)selectedObj;
//        
//        MaplyCoordinate location;
    
//        if ([theVector centroid:&location])
//        {
//            NSString *subtitle = (NSString *)theVector.userObject;
//            actualCountryLabel = [[MaplyScreenLabel alloc]init];
//            actualCountryLabel.text = [[selectedCountry attributes] objectForKey:@"ADMIN"];
//            actualCountryLabel.loc = [selectedCountry center];
//            actualCountryLabel.iconSize = CGSizeMake(100, 100);
//            NSArray *label = [NSArray arrayWithObject:actualCountryLabel];
//            //[theViewC addLabels:label desc:nil];
        //[theViewC.view addSubview: actualCountryLabel];
            //[self addAnnotation:@"" withSubtitle:subtitle at:location];
        //}
}

-(void) globeViewController:(WhirlyGlobeViewController *)viewC
                   didSelect:(NSObject *)selectedObj
{
    //[theViewC removeObject:highlightedSelectedCountry];

    
    if (!gameOver) {
        selectedCountry = (MaplyVectorObject *)selectedObj;
        [self handleSelection:viewC selected:selectedObj];
        
        [self update:true];
    }


   
}

-(void) refreshCountry
{
    [theViewC removeObject:highlightedSelectedCountry];
    [theViewC removeObject:highlightedActualCountry];
    [theViewC removeObject:countrylabels];
    //[theViewC clearAnnotations];
}

-(void) newCountry
{
    int randomCountry = arc4random()%228+1;
    
    NSArray *allOutlines = [[NSBundle mainBundle] pathsForResourcesOfType:@"geojson" inDirectory:@"country_json_50m"];
    
    NSString *outlineFile = allOutlines[randomCountry];
    
    NSData *jsonData = [NSData dataWithContentsOfFile:outlineFile];
    
    if (jsonData)
    {
        actualCountry = [MaplyVectorObject VectorObjectFromGeoJSON:jsonData];
        
        // the admin tag from the country outline geojson has the country name ­ save
        NSString *vecName = [[actualCountry attributes] objectForKey:@"ADMIN"];
        actualCountry.userObject = vecName;
        
        
        
        Country.text = vecName;

        labelsize = [Country.text boundingRectWithSize:CGSizeMake(self.view.frame.size.width, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:Country.font.pointSize weight:1.0]} context:nil];
        
        Country.frame = CGRectMake(0, 25, labelsize.size.width, 60);
        Country.center = CGPointMake(theViewC.view.center.x, 50);
        
    }
}

-(void) updateScore
{
    
    [self mapScoreValue];
    
    NSLog(@"DISTANCE = %f", kilometers);
    
    //totalPossible = totalPossible +100;

    Score.text = [NSString stringWithFormat:@"%d  /  %d", value, totalPossible];
    labelsize2 = [Score.text boundingRectWithSize:CGSizeMake(theViewC.view.frame.size.width, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:Score.font.pointSize weight:1.0]} context:nil];
    Score.frame = CGRectMake(0, 0, labelsize2.size.width, labelsize2.size.height);
    Score.center = CGPointMake(theViewC.view.center.x, theViewC.view.frame.size.height*0.9333);
    
    
    [theViewC animateToPosition:[actualCountry center] height:1.0 heading:1.0 time:animation_time-0.5];
}

-(void) mapScoreValue
{
    double number = MaplyGreatCircleDistance(actualCountry.center, selectedCountry.center);
    number = number / 1000;
    kilometers = number;
    
    double  inMin = 0;
    double  inMax = 20000;
    
    double  outMin = 100;
    double  outMax = 10;
    
    double out;
    
    if (number<=10) {
        out=100;
    }
    else
    {
        out = outMin + (outMax - outMin) * (kilometers - inMin) / (inMax - inMin);
    }
    value = value + (int)out;
}

-(void)update:(BOOL)usertapped
{
    number_of_countries ++;
    
    if (usertapped==true) {
            NSString *name = (NSString *)selectedCountry.userObject;
            
            if ([[[actualCountry attributes] objectForKey:@"ADMIN"] isEqualToString:name]) {
                vectorDict = @{
                               kMaplyColor: [UIColor greenColor],
                               kMaplySelectable: @(true),
                               kMaplyFilled: @(true),
                               kMaplyVecWidth: @(1.0)};
            }
            else{
                vectorDict = @{
                               kMaplyColor: [UIColor redColor],
                               kMaplySelectable: @(true),
                               kMaplyFilled: @(true),
                               kMaplyVecWidth: @(1.0)};
            }
            
            highlightedSelectedCountry = [theViewC addVectors:[NSArray arrayWithObject:selectedCountry] desc:vectorDict];
            
            highlightedActualCountry = [theViewC addVectors:[NSArray arrayWithObject:actualCountry] desc:vectorDict2];
            
            [self updateScore];
        
        
        [self addlabelsWithActualCountry:[[actualCountry attributes] objectForKey:@"ADMIN"] withCoordinates:actualCountry.center selectedCountry:[[selectedCountry attributes] objectForKey:@"ADMIN"] withCoordinates:selectedCountry.center];
        
        time_Sec = 10;
        
        [onesectimer invalidate];
        //[tensectimer invalidate];
        [self performSelector:@selector(idontknow) withObject:nil afterDelay:animation_time-1];
        [self performSelector:@selector(testing) withObject:nil afterDelay:animation_time];
        //[self performSelector:@selector(sec_update) withObject:nil afterDelay:animation_time];
        [self performSelector:@selector(newCountry) withObject:nil afterDelay:animation_time];
        [self performSelector:@selector(refreshCountry) withObject:nil afterDelay:animation_time];
        
        [theViewC.view setUserInteractionEnabled:false];
        
    }
    else
    {
        time_Sec = 10
        ;
        [onesectimer invalidate];
        //[tensectimer invalidate];
        [self performSelector:@selector(idontknow) withObject:nil afterDelay:0];
        [self performSelector:@selector(testing) withObject:nil afterDelay:0];
        //[self performSelector:@selector(sec_update) withObject:nil afterDelay:0];
        [self performSelector:@selector(newCountry) withObject:nil afterDelay:0];
        [self performSelector:@selector(refreshCountry) withObject:nil afterDelay:0];
    }
    
    
    
    if (number_of_countries>gamenumber) {
        gameOver=true;
        NSLog(@"gameover");
    }
}

-(void) testing
{
    [theViewC.view setUserInteractionEnabled:true];
    
    //tensectimer = [NSTimer scheduledTimerWithTimeInterval:9 target:self selector:@selector(update:) userInfo:nil repeats:YES];
}

-(void) sec_update
{
    
    if (gameOver==false) {
        if (time_Sec <=0) {
            time_Sec=10;
            updateTime = true;
        }
        
        if (updateTime) {
            updateTime = false;
            [self update:false];
        }
        
        [self update_Timer];
        
        time_Sec--;
    }
    else
    {
        [startGame setHidden:false];
        [onesectimer invalidate];
        [Country removeFromSuperview];
        [Timing removeFromSuperview];
        //[Score removeFromSuperview];
        
        [theViewC setAutoRotateInterval:0.1 degrees:60];
    }

}
    
-(void) update_Timer
{
    Timing.text = [NSString stringWithFormat:@"%d", time_Sec];
    labelsize3 = [Timing.text boundingRectWithSize:CGSizeMake(theViewC.view.frame.size.width, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:Timing.font.pointSize weight:1.0]} context:nil];
    
    Timing.frame = CGRectMake(0, 0, labelsize3.size.width, labelsize3.size.height);
    Timing.center = CGPointMake(theViewC.view.frame.size.width*0.9, theViewC.view.frame.size.height*0.9333);
  
}


-(void)addlabelsWithActualCountry:(NSString *)actualCountry withCoordinates:(MaplyCoordinate)actualcoord selectedCountry:(NSString *)selectedCountry withCoordinates:(MaplyCoordinate)selectedcoord
{
    actualCountryLabel = [[MaplyLabel alloc]init];
    actualCountryLabel.text = actualCountry;
    actualCountryLabel.color = [UIColor blackColor];
    actualCountryLabel.loc = actualcoord;
    actualCountryLabel.size = CGSizeMake(0.2, 0.2);
    
    selectedCountryLabel = [[MaplyLabel alloc]init];
    selectedCountryLabel.text = selectedCountry;
    selectedCountryLabel.color = [UIColor blackColor];
    selectedCountryLabel.loc = selectedcoord;
    selectedCountryLabel.size = CGSizeMake(0.2, 0.2);
    
    labels = [NSArray arrayWithObjects:actualCountryLabel, selectedCountryLabel, nil];
    countrylabels = [[MaplyComponentObject alloc]init];
    countrylabels = [theViewC addLabels:labels desc:nil];
}

@end
