//
//  main.m
//  HelloEarth
//
//  Created by Rehan Anwar on 2016-04-25.
//  Copyright © 2016 Rehan Anwar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
